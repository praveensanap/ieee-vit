
var CT = require('./modules/country-list');
var AM = require('./modules/account-manager');
var EM = require('./modules/email-dispatcher');

module.exports = function(app) {

    app.get('/',function(req,res){
	res.render('index');
    });

    app.get('/event/minixtreme',function(req,res){
	res.render('minixtreme');
    });

    app.get('/event/cryptocrack',function(req,res){
	res.render('cryptocrack');
    });
    app.get('/event/arduino',function(req,res){
	res.render('arduino');
    });

    app.get('/event/RACEM',function(req,res){
	res.render('RACEM');
    });
    app.get('/RACEM/E-YANTRA-WS',function(req,res){
	res.render('eyantraworkshop');
    });

    app.get('/RACEM/MICROWAVE-CAD-TOOLS',function(req,res){
	res.render('microwavecad');
    });
    app.get('/RACEM/USABILITY-ENGINEERING',function(req,res){
	res.render('usabilityengg');
    });
    app.get('/RACEM/ANDROID-APPLICATION-DEVELOPMENT',function(req,res){
	res.render('androidappdev');
    });
    app.get('/RACEM/OSCAD',function(req,res){
	res.render('oscad');
    });

    app.get('/event/',function(req,res){
	res.render('cryptocrack');
    });


    app.get('/event/technodox',function(req,res){
	res.render('technodox');
    });

    app.get('/event/clashoftitans',function(req,res){
	res.render('clashoftitans');
    });
    
    app.get('/aboutieee',function(req,res){
	res.render('aboutieee');
    });

    app.get('/team',function(req,res){
	res.render('team');
    });

// Portal
//Cryptocrack
    app.get('/portal/cryptocrack',function(req,res){
	// check if the user's credentials are saved in a cookie //
	if (req.cookies.user == undefined || req.cookies.pass == undefined){
	    res.redirect('/event/cryptocrack');
	}	else{
	    // attempt automatic login //
	    AM.autoLogin(req.cookies.user, req.cookies.pass, function(o){
		if (o != null){
		    req.session.user = o;
		    res.redirect('/portal/cryptocrack/dashboard');
		}	else{
		    res.redirect('/event/cryptocrack');
		}
	    });
	}
    });
    app.get('/portal/cryptocrack/dashboard',function(req,res){
	if (req.session.user == null){
	    // if user is not logged-in redirect back to login page //
	    res.redirect('/event/cryptocrack');
	  
	    console.log('sdafsa');
	} else{
	    question = AM.question(req.session.user.team_id);
	    console.log("logfged in ");
	    res.render('ccdash', {
		user : req.session.user,
		question : question
	    });
	}

    });
    app.get('/portal/cryptocrack/otp',function(req,res){
	if (req.cookies.user == undefined || req.cookies.pass == undefined){
	    res.redirect('/event/cryptocrack');
	}	else{
	    // attempt automatic login //
	    AM.autoLogin(req.cookies.user, req.cookies.pass, function(o){
		if (o != null){
		    req.session.user = o;
		    res.render('ccotp');
		}	else{
		    res.redirect('/event/cryptocrack');
		}
	    });
	}
    });

    app.get('/portal/cryptocrack/submissions',function(req,res){
	if (req.cookies.user == undefined || req.cookies.pass == undefined){
	    res.redirect('/event/cryptocrack');
	}	else{
	    // attempt automatic login //
	    AM.autoLogin(req.cookies.user, req.cookies.pass, function(o){
		if (o != null){
		    req.session.user = o;
		    res.render('ccsub',{user:o});
		}	else{
		    res.redirect('/event/cryptocrack');
		}
	    });
	}
    });
    
    app.post('/portal/cryptocrack/gethint',function(req,res){
	if (req.cookies.user == undefined || req.cookies.pass == undefined){
	    res.redirect('/event/cryptocrack');
	}	else{
	    // attempt automatic login //
	    AM.autoLogin(req.cookies.user, req.cookies.pass, function(o){
		if (o != null){
		    req.session.user = o;
		    AM.updateHint(o,function(e){
			res.redirect('/portal/cryptocrack');
		    });
		}	else{
		    res.redirect('/event/cryptocrack');
		}
	    });
	}
    });
    app.post('/portal/cryptocrack/submit',function(req,res){
	if (req.cookies.user == undefined || req.cookies.pass == undefined){
	    res.redirect('/event/cryptocrack');
	}	else{
	    // attempt automatic login //
	    AM.autoLogin(req.cookies.user, req.cookies.pass, function(o){
		if (o != null){
		    req.session.user = o;
		    a=req.param('m1');
		    b=req.param('m2');
		    AM.updateSubmit(o,a,b,function(e){
			res.redirect('/portal/cryptocrack/submissions');
		    });
		}	else{
		    res.redirect('/event/cryptocrack');
		}
	    });
	}
    });
    app.get('/portal/cryptocrack/rules',function(req,res){
	if (req.cookies.user == undefined || req.cookies.pass == undefined){
	    res.redirect('/event/cryptocrack');
	}	else{
	    // attempt automatic login //
	    AM.autoLogin(req.cookies.user, req.cookies.pass, function(o){
		if (o != null){
		    req.session.user = o;
		    res.render('ccrules');
		}	else{
		    res.redirect('/event/cryptocrack');
		}
	    });
	}
    });
    

//Minixtreme
    app.get('/portal/minixtreme/dashboard',function(req,res){
	res.render('medash');
    });
    app.get('/portal/minixtreme/question/:x',function(req,res){
	res.render('medash');
    });
    app.get('/portal/minixtreme/submissions',function(req,res){
	res.render('mesub');
    });
    app.get('/portal/minixtreme/rules',function(req,res){
	res.render('merules');
    });

    app.get('/portal/minixtreme/', function(req, res){
	// check if the user's credentials are saved in a cookie //
	if (req.cookies.user == undefined || req.cookies.pass == undefined){
	    res.render('login', { title: 'Hello - Please Login To Your Account' });
	}	else{
	// attempt automatic login //
	    AM.autoLogin(req.cookies.user, req.cookies.pass, function(o){
		if (o != null){
		    req.session.user = o;
		    res.redirect('/portal/minixtreme/dashboard');
		}	else{
		    res.render('login', { title: 'Hello - Please Login To Your Account' });
		}
	    });
	}
    });
   
    app.post('/portal/login', function(req, res){
	AM.manualLogin(req.param('user'), req.param('pass'), function(e, o){
	    if (!o){
		res.send(e, 400);
	    }	else{
		req.session.user = o;
		res.cookie('user', o.username, { maxAge: 900000 });
		res.cookie('pass', o.pass, { maxAge: 900000 });
		res.redirect('/portal/cryptocrack/dashboard');
	    }
	});
    });
	
// logged-in user homepage //
	
    app.get('/home', function(req, res) {
	if (req.session.user == null){
	    // if user is not logged-in redirect back to login page //
	    res.redirect('/');
	}   else{
	    res.render('home', {
		title : 'Control Panel',
		countries : CT,
		udata : req.session.user
	    });
	}
    });
    
    app.post('/home', function(req, res){
	if (req.param('user') != undefined) {
	    AM.updateAccount({
		user 		: req.param('user'),
		name 		: req.param('name'),
		email 		: req.param('email'),
		country 	: req.param('country'),
		pass		: req.param('pass')
	    }, function(e, o){
		if (e){
		    res.send('error-updating-account', 400);
		}	else{
		    req.session.user = o;
		    // update the user's login cookies if they exists //
		    if (req.cookies.user != undefined && req.cookies.pass != undefined){
			res.cookie('user', o.user, { maxAge: 900000 });
			res.cookie('pass', o.pass, { maxAge: 900000 });	
		    }
		    res.send('ok', 200);
		}
	    });
	}	else if (req.param('logout') == 'true'){
	    res.clearCookie('user');
	    res.clearCookie('pass');
	    req.session.destroy(function(e){ res.send('ok', 200); });
	}
    });
    
    // creating new accounts //
/*    
    app.get('/signup', function(req, res) {
	for(var i=0;i< users.length ;i++){
            AM.addNewAccount({
		team_id     : i,
		username : users[i],
		pass : passwd[i],
		intime: new Date(),
		starttime: new Date(),
		minicode1:{problem_id:0,
			   lang:'',
			   solution:''},
		minicode2:{problem_id:0,
			   lang:'',
			   solution:''},
		minicode3:{problem_id:0,
			   lang:'',
			   solution:''},
		minicode4:{problem_id:0,
			   lang:'',
			   solution:''},
		minicode5:{problem_id:0,
			   lang:'',
			   solution:''},
		minicode6:{problem_id:0,
			   lang:'',
			   solution:''},
		minicode7:{problem_id:0,
			   lang:'',
			   solution:''},
		minicode8:{problem_id:0,
			   lang:'',
			   solution:''},
		minicode9:{problem_id:0,
			   lang:'',
			   solution:''},
		minicode10:{problem_id:0,
			    lang:'',
			    solution:''},
		minicode11:{problem_id:0,
			    lang:'',
			    solution:''},
		minicode12:{problem_id:0,
			    lang:'',
			    solution:''},
		ccmessage:{message1:'',message2:''},
		hint:false,
		expired:false
	    }, function(e){
		if (e){
		    res.send(e, 400);
		} else{
		    res.send('ok', 200);
		}
	    });
	    console.log(i);
        }
	res.render('signup');
    });
    
    app.post('/signup', function(req, res){
	AM.addNewAccount({
	    team_id     : req.param('team_id'),
	    username 	: req.param('username'),
	    pass	: req.param('pass'),
	    intime: new Date(),
	    starttime: new Date(),
	    minicode1:{problem_id:0,
		       lang:'',
		       solution:''},
	    minicode2:{problem_id:0,
		       lang:'',
		       solution:''},
	    minicode3:{problem_id:0,
		       lang:'',
		       solution:''},
	    minicode4:{problem_id:0,
		       lang:'',
		       solution:''},
	    minicode5:{problem_id:0,
		       lang:'',
		       solution:''},
	    minicode6:{problem_id:0,
		       lang:'',
		       solution:''},
	    minicode7:{problem_id:0,
		       lang:'',
		       solution:''},
	    minicode8:{problem_id:0,
		       lang:'',
		       solution:''},
	    minicode9:{problem_id:0,
		       lang:'',
		       solution:''},
	    minicode10:{problem_id:0,
			lang:'',
			solution:''},
	    minicode11:{problem_id:0,
			lang:'',
			solution:''},
	    minicode12:{problem_id:0,
			lang:'',
			solution:''},
	    ccmessage:{message1:'',message2:''},
	    hint:false,
	    expired:false
	}, function(e){
	    if (e){
		res.send(e, 400);
	    }	else{
		res.send('ok', 200);
	    }
	});
    });
*/    
    // password reset //
    
    app.post('/lost-password', function(req, res){
	// look up the user's account via their email //
	AM.getAccountByEmail(req.param('email'), function(o){
	    if (o){
		res.send('ok', 200);
		EM.dispatchResetPasswordLink(o, function(e, m){
		    // this callback takes a moment to return //
		    // should add an ajax loader to give user feedback //
		    if (!e) {
			//	res.send('ok', 200);
		    }	else{
			res.send('email-server-error', 400);
			for (k in e) console.log('error : ', k, e[k]);
		    }
		});
	    }	else{
		res.send('email-not-found', 400);
	    }
	});
    });
    
    app.get('/reset-password', function(req, res) {
	var email = req.query["e"];
	var passH = req.query["p"];
	AM.validateResetLink(email, passH, function(e){
	    if (e != 'ok'){
		res.redirect('/');
	    } else{
		// save the user's email in a session instead of sending to the client //
		req.session.reset = { email:email, passHash:passH };
		res.render('reset', { title : 'Reset Password' });
	    }
	})
    });
    
    app.post('/reset-password', function(req, res) {
	var nPass = req.param('pass');
	// retrieve the user's email from the session to lookup their account and reset password //
	var email = req.session.reset.email;
	// destory the session immediately after retrieving the stored email //
	req.session.destroy();
	AM.updatePassword(email, nPass, function(e, o){
	    if (o){
		res.send('ok', 200);
	    }	else{
		res.send('unable to update password', 400);
	    }
	})
    });
    
    // view & delete accounts //
    
    app.get('/print', function(req, res) {
	AM.getAllRecords( function(e, accounts){
	    res.render('print', { title : 'Account List', accts : accounts });
	})
    });
    
    app.post('/delete', function(req, res){
	AM.deleteAccount(req.body.id, function(e, obj){
	    if (!e){
		res.clearCookie('user');
		res.clearCookie('pass');
	        req.session.destroy(function(e){ res.send('ok', 200); });
	    }	else{
		res.send('record not found', 400);
	    }
	});
    });
    
    app.get('/reset', function(req, res) {
	AM.delAllRecords(function(){
	    res.redirect('/print');	
	});
    });
    
    app.get('*', function(req, res) { res.send('404'); });
    
}
