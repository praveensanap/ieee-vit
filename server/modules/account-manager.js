
var crypto 		= require('crypto');
var MongoDB 	= require('mongodb').Db;
var Server 		= require('mongodb').Server;
var moment 		= require('moment');
var mongoose        = require('mongoose');

/* establish the database connection */

var mongourl ='mongodb://123test:123test@ds027779.mongolab.com:27779/ieee-vit';
mongoose.connect(mongourl, function (err, res) {
  if (err) {
      console.log ('ERROR connecting to: ' + mongourl + '. ' + err);
  } else {
      console.log ('Succeeded connected to: ' + mongourl);
  }
});


q= ['1100101110111011001011101101111100101000001110011110100011001011110010110110011011111100011110101101000001101000110111111011001101101110010111100110100000010000001000000100000', '1110000111001011011111100110110010111100111110011110111111100100100000110101011010011101101010000011011011101111111001011010011100001111001011101001111001010000001000000100000', '1101011110100111011001101100010000011101001101000110010101000001100001110110111000101100001111001111100111100001110010011011111110010010000001000000100000010000001000000100000', '1110011111010111100001110000110110011110010100000110011111101011101110111001101000001110100110111101000001100111110010111100101101101110000111011101111001010000001000000100000', '1101101110111111011101100101111100101000001100001111010001000001100010110000111011101101011010000011011111100110010000011110101110101111001011010011100011110100001000000100000', '1100001110011111001011101110111010001000001100001111010001000001110011110001111011111110100110110011000011101110110010001000001111001110000111100101100100010000001000000100000', '1100001111010011101001100001110001111010110100000111010011010001100101010000011100001100001111001011011001101001110000111011011100101110111011101000100000010000001000000100000', '1110111110000111101001100011110100001000001100110110111111100100100000110100111100101100101110111011001010100000110000111001001101100110010111100100100000010000001000000100000', '1101000110000111000111101011010000011001111101111111011011001011110010110111011011011100101110111011101000100000110011011010011101100110010111100110100000010000001000000100000', '1101101110000111100111110011110001011100101100101110000111010110100000110110011011111101110110010011011111101110010000011010101100001110100111011000100000010000001000000100000']

/* Accounts Schema*/

var accountSchema = new mongoose.Schema({
    team_id: Number,
    username: String,
    pass: String,
    intime: Date,
    starttime:Date,
    minicode1:{problem_id:Number,
	       lang:String,
	       solution:String},
    minicode2:{problem_id:Number,
	       lang:String,
	       solution:String},
    minicode3:{problem_id:Number,
	       lang:String,
	       solution:String},
    minicode4:{problem_id:Number,
	       lang:String,
	       solution:String},
    minicode5:{problem_id:Number,
	       lang:String,
	       solution:String},
    minicode6:{problem_id:Number,
	       lang:String,
	       solution:String},
    minicode7:{problem_id:Number,
	       lang:String,
	       solution:String},
    minicode8:{problem_id:Number,
	       lang:String,
	       solution:String},
    minicode9:{problem_id:Number,
	       lang:String,
	       solution:String},
    minicode10:{problem_id:Number,
		lang:String,
		solution:String},
    minicode11:{problem_id:Number,
		lang:String,
		solution:String},
    minicode12:{problem_id:Number,
		lang:String,
		solution:String},
    ccmessage:{message1:String,message2:String},
    hint:Boolean,
    expired:Boolean

});

var accounts = mongoose.model('accounts',accountSchema);

/* login validation methods */

exports.autoLogin = function(user, pass, callback){
	accounts.findOne({username:user}, function(e, o) {
		if (o){
			o.pass == pass ? callback(o) : callback(null);
		}	else{
			callback(null);
		}
	});
}

exports.manualLogin = function(user, pass, callback)
{
	accounts.findOne({username:user}, function(e, o) {
		if (o == null){
			callback('user-not-found');
		}	else{
			validatePassword(pass, o.pass, function(err, res) {
				if (res){
					callback(null, o);
				}	else{
					callback('invalid-password');
				}
			});
		}
	});
}

/* record insertion, update & deletion methods */

exports.addNewAccount = function(newData, callback)
{
    accounts.findOne({user:newData.username}, function(e, o) {
		if (o){
			callback('username-taken');
		}else{
		    saltAndHash(newData.pass, function(hash){
			newData.pass = hash;
			// append date stamp when record was created //
			newData.date = moment().format('MMMM Do YYYY, h:mm:ss a');
			accounts.create(newData, callback);
		    });
		}
    });
}

exports.question = function(x){
    question = {m1:'',m2:''};
    question.m1 = q[x*x%10];
    question.m2 = q[x*x*x%10];
    return question
}
exports.updateHint = function(newData, callback){
    accounts.update({username:newData.username}, {$set: { "hint":true}},{multi:true},function(err){
	callback(err)
    });
}
exports.updateSubmit = function(newData,a,b, callback){
    accounts.update({username:newData.username}, 
		    {$set: {'ccmessage':{'message1':a,'message2':b }}},
		    {multi:true},function(err){
			callback(err)
    });
}
//update({user:newData.user}, {hint:true},function(err){
//	return
  //  });
		   
		  
/*
exports.updatePassword = function(email, newPass, callback)
{
	accounts.findOne({email:email}, function(e, o){
		if (e){
			callback(e, null);
		}	else{
			saltAndHash(newPass, function(hash){
		        o.pass = hash;
		        accounts.save(o, {safe: true}, callback);
			});
		}
	});
}

/* account lookup methods */

exports.deleteAccount = function(id, callback)
{
	accounts.remove({_id: getObjectId(id)}, callback);
}

exports.getAccountByEmail = function(email, callback)
{
	accounts.findOne({email:email}, function(e, o){ callback(o); });
}

exports.validateResetLink = function(email, passHash, callback)
{
	accounts.find({ $and: [{email:email, pass:passHash}] }, function(e, o){
		callback(o ? 'ok' : null);
	});
}

exports.getAllRecords = function(callback)
{
	accounts.find().toArray(
		function(e, res) {
		if (e) callback(e)
		else callback(null, res)
	});
};

exports.delAllRecords = function(callback)
{
	accounts.remove({}, callback); // reset accounts collection for testing //
}

/* private encryption & validation methods */

var generateSalt = function()
{
	var set = '0123456789abcdefghijklmnopqurstuvwxyzABCDEFGHIJKLMNOPQURSTUVWXYZ';
	var salt = '';
	for (var i = 0; i < 10; i++) {
		var p = Math.floor(Math.random() * set.length);
		salt += set[p];
	}
	return salt;
}

var md5 = function(str) {
	return crypto.createHash('md5').update(str).digest('hex');
}

var saltAndHash = function(pass, callback)
{
	var salt = generateSalt();
	callback(salt + md5(pass + salt));
}

var validatePassword = function(plainPass, hashedPass, callback)
{
	var salt = hashedPass.substr(0, 10);
	var validHash = salt + md5(plainPass + salt);
	callback(null, hashedPass === validHash);
}

/* auxiliary methods */

var getObjectId = function(id)
{
	return accounts.db.bson_serializer.ObjectID.createFromHexString(id)
}

var findById = function(id, callback)
{
	accounts.findOne({_id: getObjectId(id)},
		function(e, res) {
		if (e) callback(e)
		else callback(null, res)
	});
};


var findByMultipleFields = function(a, callback)
{
// this takes an array of name/val pairs to search against {fieldName : 'value'} //
	accounts.find( { $or : a } ).toArray(
		function(e, results) {
		if (e) callback(e)
		else callback(null, results)
	});
}
