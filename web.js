var express = require('express');
var http = require('http');
var app = express();

app.configure(function(){
    app.set("port",process.env.PORT||3030);
    app.set("views",__dirname + '/views');
    app.set("view engine","jade");

    app.use(express.logger());
    app.use(express.bodyParser());
    app.use(express.methodOverride());
    app.use(express.cookieParser());
    app.use(express.session({ secret: 'super-duper-secret-secret' }));
    app.use(express.static(__dirname + '/public'));
    app.use(app.router);
});

require('./server/router')(app);

http.createServer(app).listen(app.get('port'), function(){
  console.log('Express server listening on port ' + app.get('port'));
});
